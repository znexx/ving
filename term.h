#ifndef TERM_H
#define TERM_H

#include <stdbool.h>

void push_origin (int, int);
void pop_origin (void);

void clear_screen(void);
void show_cursor (bool state);

void set_bold (bool state);
void set_underline (bool state);
void set_blinking (bool state);
void set_negative (bool state);
void set_reverse_video (bool state);
void set_origin_mode (bool state);
void cursor_goto (int, int);

#endif
