#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "fill_widget.h"
#include "term.h"

static void render (void *widget, int x, int y, int w, int h) {
	FillWidget_t *fill_widget = widget;
	for (int py = y; py < y + h; py++) {
		for (int px = x; px < x + w; px++) {
			cursor_goto(px, py);
			printf("%c", fill_widget->fill_char);
		}
	}
	fill_widget->needs_redraw = false;
}

FillWidget_t *create_fill_widget(char fill_char) {
	Widget_t *parent = create_widget(&render, NULL, NULL);
	parent->focusable = false;
	FillWidget_t *fill_widget = realloc(parent, sizeof(FillWidget_t));

	// FillWidget fields
	fill_widget->fill_char = fill_char;
	return fill_widget;
}
