#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "grid_widget.h"
#include "term.h"

static void render (void *widget, int x, int y, int w, int h) {
	GridWidget_t *grid_widget = widget;

	int x1 = x;
	int y1 = y;

	int x2 = 0;
	int y2 = 0;

	for(int row = 0; row < grid_widget->n_rows; row++) {
		y2 = y + ((row + 1) * h) / grid_widget->n_rows;
		for(int column = 0; column < grid_widget->n_columns; column++) {
			x2 = x + ((column + 1) * w) / grid_widget->n_columns;

			Widget_t *child = grid_widget->children[column][row];

			if (child) {
				if (child->needs_redraw) {
					child->render(child, x1, y1, x2 - x1, y2 - y1);
				}
			} else {
				if (!grid_widget->grid_drawn && grid_widget->needs_redraw) {
					cursor_goto(x1, y1);
					putchar('+');
					for(int x = x1 + 1; x < x2 - 1; x++) {
						cursor_goto(x, y1);
						putchar('-');
						cursor_goto(x, y2 - 1);
						putchar('-');
					}
					for (int y = y1 + 1; y < y2 - 1; y++) {
						cursor_goto(x1, y);
						putchar('|');
						cursor_goto(x2 - 1, y);
						putchar('|');
					}
					cursor_goto(x1, y2 - 1);
					putchar('+');
					cursor_goto(x2 - 1, y1);
					putchar('+');
					cursor_goto(x2 - 1, y2 - 1);
					putchar('+');
				}
			}

			x1 = x2;
		}
		x1 = x;
		y1 = y2;
	}
	grid_widget->grid_drawn = true;
	grid_widget->needs_redraw = false;
}

void set_grid_child(GridWidget_t *grid_widget, void *widget, int row, int column) {
	grid_widget->children[column][row] = widget;
}

static void free_grid_widget (void *_widget) {
	GridWidget_t *widget = _widget;

	for (int column = 0; column < widget->n_columns; column++) {
		for (int row = 0; row < widget->n_rows; row++) {
			Widget_t *child = widget->children[column][row];
			if (child) {
				child->free(child);
			}
		}
		free(widget->children[column]);
	}
	free(widget->children);

	free(widget);
}

void grid_focus_first (GridWidget_t *grid_widget) {
	grid_widget->has_focus = true;
	grid_widget->needs_redraw = true;

	Widget_t *child = NULL;
	int row = 0, column = 0;
	do {
		child = grid_widget->children[column][row];
		if (child && child->focusable) {
			if (child->type == GRID_WIDGET) {
				grid_focus_first((GridWidget_t *)child);
			} else {
				child->has_focus = true;
				child->needs_redraw = true;
			}
			return;
		}
	} while (!step_2d(&column, &row, grid_widget->n_columns, grid_widget->n_rows));
}

static bool focus_next (GridWidget_t *grid_widget) {
	Widget_t *current_focused = NULL;
	Widget_t *child = NULL;
	int row = 0, column = 0;
	do {
		child = grid_widget->children[column][row];
		if (child && child->focusable && child->has_focus) {
			current_focused = child;
			current_focused->has_focus = false;
			current_focused->needs_redraw = true;
			break;
		}
	} while (!step_2d(&column, &row, grid_widget->n_columns, grid_widget->n_rows));

	if (child == NULL) {
		// no elements in grid
		return false;
	}

	if (current_focused == NULL) {
		// only non-focused or non-focusable elements were found and we are at (0, 0) so keep focusing from there
	} else if (current_focused->type == GRID_WIDGET) {
		if (focus_next((GridWidget_t *)current_focused)) {
			current_focused->has_focus = true;
			current_focused->needs_redraw = true;
			return true;
		}
	}

	while (!step_2d(&column, &row, grid_widget->n_columns, grid_widget->n_rows)) {
		child = grid_widget->children[column][row];

		if (child && child->focusable) {
			// unfocus all other later focused element, only one should be able to be focused
			if (child->type == GRID_WIDGET) {
				if (focus_next((GridWidget_t *)child)) {
					child->has_focus = true;
					child->needs_redraw = true;
					return true;
				}
			}
			if (child->has_focus) {
				child->has_focus = false;
				child->needs_redraw = true;
			} else {
				child->has_focus = true;
				child->needs_redraw = true;
				return true;
			}
		}
	}

	grid_widget->has_focus = false;
	grid_widget->needs_redraw = true;
	return false;
}

static void use_character(void *widget, int character) {
	GridWidget_t *grid_widget = widget;

	// escape
	if (character == 0x1b) {
		return;
	}

	// Tab button
	if (character == 0x09) {
		focus_next(grid_widget);
		grid_widget->needs_redraw = true;
		return;
	}

	int row = 0, column = 0;
	do {
		Widget_t *child = grid_widget->children[column][row];
		if (child && child->has_focus) {
			child->use_character(child, character);
			if (child->needs_redraw) {
				grid_widget->needs_redraw = true;
			}
		}
	} while (!step_2d(&column, &row, grid_widget->n_columns, grid_widget->n_rows));

}

GridWidget_t *create_grid_widget (int rows, int columns) {
	Widget_t *parent = create_widget(&render, &free_grid_widget, &use_character);
	GridWidget_t *grid_widget = realloc(parent, sizeof(GridWidget_t));
	grid_widget->type = GRID_WIDGET;

	// GridWidget fields
	grid_widget->n_rows = rows;
	grid_widget->n_columns = columns;
	grid_widget->children = calloc(columns, sizeof(GridWidget_t **));
	for (int n = 0; n < columns; n++) {
		grid_widget->children[n] = calloc(rows, sizeof(GridWidget_t *));
	}
	grid_widget->grid_drawn = false;

	return grid_widget;
}
