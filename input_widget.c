#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "input_widget.h"
#include "term.h"

static void render (void *_widget, int x, int y, int w, int h) {
	InputWidget_t *widget = _widget;
	int real_estate = w * h - 1;
	cursor_goto(x, y);
	int dx = 0;
	int dy = 0;

	int i = widget->buffer_ptr - real_estate;
	if (i < 0) {
		i = 0;
	}
	while (widget->buffer[i] != '\0' && i < widget->buffer_size) {
		int character = widget->buffer[i];

		cursor_goto(x + dx, y + dy);
		if (character == '\n') {
			dx = w;
			character = ' ';
		}
		putchar(character);

		if (step_2d(&dx, &dy, w, h)) {
			return;
		}
		i++;
	}

	cursor_goto(x + dx, y + dy);
	if (widget->has_focus) {
		set_blinking(true);
		cursor_goto(x + dx, y + dy);
		putchar('_');
		set_blinking(false);
	} else {
		putchar(' ');
	}

	while(!step_2d(&dx, &dy, w, h)) {
		cursor_goto(x + dx, y + dy);
		putchar(' ');
	}

	widget->needs_redraw = false;
}

static void use_character (void *_widget, int character) {
	InputWidget_t *widget = _widget;

	if (character == 0x08 || character == 0x7f) { // backspace
		if (widget->buffer_ptr >= 1) {
			widget->buffer_ptr--;
			widget->buffer[widget->buffer_ptr] = '\0';
		}
	} else if (character == 0xff) { // delete
		if (widget->buffer_ptr >= 1) {
			widget->buffer_ptr--;
			widget->buffer[widget->buffer_ptr] = '\0';
		}
	} else {
		widget->buffer[widget->buffer_ptr] = character;
		widget->buffer_ptr++;
	}

	widget->needs_redraw = true;
}

static void free_widget (void *_widget) {
	InputWidget_t *widget = _widget;
	free(widget->buffer);
	free(widget);
}

InputWidget_t *create_input_widget() {
	Widget_t *parent = create_widget(&render, &free_widget, &use_character);
	InputWidget_t *input_widget = realloc(parent, sizeof(InputWidget_t));

	// InputWidget fields
	input_widget->buffer_ptr = 0;
	input_widget->buffer_size = 255;
	input_widget->buffer = calloc(input_widget->buffer_size, sizeof(char));
	return input_widget;
}
