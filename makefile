SHELL=/bin/bash
CC=gcc
CFLAGS=-std=c11 -fms-extensions -pedantic -I. -Wall -Wextra -Og -g
LDFLAGS=-L/usr/local/lib -lpthread -pedantic -Wall -Wextra
PROF=valgrind
PROFFLAGS=--tool=callgrind
PROFVIS=kcachegrind

SRC=$(wildcard *.c)
OBJS=$(SRC:.c=.o)
TEST_SRC=$(wildcard tests/*.c)
TEST_OBJS=$(patsubst %.c, %.o, $(TEST_SRC))
TEST_BINS=$(patsubst %.c, %.test, $(TEST_SRC))

CORRECT_OUTPUTS=$(patsubst %.c, %.correct.png, $(TEST_SRC))
TEST_OUTPUTS=$(patsubst %.c, %.result, $(TEST_SRC))
LEAK_TESTS=$(patsubst %.c, %.leak-check, $(TEST_SRC))

.ONESHELL:
.PHONY: clean

default: testall

%.test: %.o $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

%.result.ans: %.test
	TERM=vt220 ./$< > $@

%.correct.png: %.result.png
	mv -n $< $@

%.result.png: %.result.ans
	ansilove -q -c 80 -o $@ $<

%.result: %.result.png
	compare -quiet -metric rmse $< $(subst .result.png,.correct.png,$<) NULL: 2> /dev/null

%.leak-check: %.test
	valgrind --quiet --leak-check=full --errors-for-leak-kinds=all --error-exitcode=1 $< >/dev/null

testall: $(TEST_OUTPUTS) $(LEAK_TESTS)
	# ************
	# ALL TESTS OK
	# ************

tests: $(TEST_BINS)

# Generates missing output images from the current state of test cases, so be careful that they are correct
fill_missing_corrects: $(CORRECT_OUTPUTS)

clean:
	rm -rf $(OBJS) $(TEST_BINS)
