#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "term.h"

/*
 * CUSTOM TERMINAL HANDLING
 */

static int *origin_stack_x;
static int *origin_stack_y;
static int origin_stack_size = 0;
static int origin_stack_head = -1;

void push_origin (int x, int y) {
	origin_stack_head++;

	if (origin_stack_head >= origin_stack_size) {
		origin_stack_size = origin_stack_head + 1;
		origin_stack_x = realloc(origin_stack_x, origin_stack_size * sizeof(int));
		origin_stack_y = realloc(origin_stack_y, origin_stack_size * sizeof(int));
	}

	origin_stack_x[origin_stack_head] = x;
	origin_stack_y[origin_stack_head] = y;

	cursor_goto(0, 0);
}

void pop_origin (void) {
	/*if (origin_stack_head >= 0) {
		return origin_stack[origin_stack_head--];
	} else {
		return (TerminalCoordinate_t) {0, 0};
	}*/

	if (origin_stack_head >= 0) {
		origin_stack_head--;

		if (origin_stack_head < 0) {
			free(origin_stack_x);
			free(origin_stack_y);
		}
	}
}

static void add_origins(int *row, int *column) {
	for (int n = 0; n <= origin_stack_head; n++) {
		(*row) += origin_stack_y[n];
		(*column) += origin_stack_x[n];
	}
}

/*
 * STANDARD VT100/VT220 CONTROL SEQUENCES
 */

void clear_screen (void) {
	printf("\033[2J");
}

void show_cursor (bool state) {
	printf("\033[?25%c", state ? 'h' : 'l');
}

static void send_rendition (const char *rendition_value) {
	puts("\033[");
	puts(rendition_value);
	puts("m");
}

void set_bold (bool state) {
	send_rendition(state ? "1" : "22");
}

void set_underline  (bool state) {
	send_rendition(state ? "4" : "24");
}

void set_blinking  (bool state) {
	send_rendition(state ? "5" : "25");
}

void set_negative (bool state) {
	send_rendition(state ? "7" : "27");
}

void set_reverse_video (bool state) {
	printf("\033[?5%c", state ? 'h' : 'l');
}

void set_origin_mode (bool state) {
	printf("\033[?6%c", state ? 'h' : 'l');
}

void cursor_goto (int x, int y) {
	add_origins(&y, &x);
	printf("\033[%d;%dH", y + 1, x + 1);
}
