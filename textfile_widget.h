#ifndef TEXTFILE_WIDGET_H
#define TEXTFILE_WIDGET_H

#include "widget.h"

typedef struct {
	Widget_t;
	FILE *text_file;
} TextfileWidget_t;

TextfileWidget_t *create_textfile_widget(const char*);

#endif
