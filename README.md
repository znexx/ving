Ving
====

Ving is a TUI (Terminal User Interface) library written in C11. It is aimed primarily at supporting VT220 compatible terminals.

Its features include:

  - A tree structure which you will recognize if you have written javascript for the DOM
  - A number of modern widgets, such as:
    - Regular buttons
    - Text input fields with support for multiple lines and input longer than can be displayed
	- A "named pipe widget" which creates a named pipe and shows what it written to it
	- Widgets filled with one character! Wanna make a rectangle of "x"'s? Use the FillWidget_t!
	- A grid layout widget with a flexible number of columns and rows - can be adjusted on the fly!
    
Examples
--------

For example code, see the C source files in the `tests/` directory, alternatively a simple example will be shown here soon. Some projects of mine which will use Ving will also be linked here.

Running tests
-------------

To run the test cases you need:

  - [GCC](https://gcc.gnu.org/) - for compiling
  - [Make](http://www.gnu.org/software/make/) - for building and running the tests
  - [Valgrind](https://www.valgrind.org/) - for memory leak tests
  - [Ansilover](https://github.com/ansilove/ansilove) - for output verification

All of these probably exist in 
 
To run all tests, just run `make`

To just compile the test case binaries, run `make tests`.