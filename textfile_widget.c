#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "textfile_widget.h"
#include "term.h"

static void render (void *_widget, int x, int y, int w, int h) {
	TextfileWidget_t *widget = _widget;

	if (widget->text_file != NULL) {
		rewind(widget->text_file);

		int lines = 0;
		int chars = 0;
		int max_chars = 0;
		int c = 0;
		long *line_positions = NULL;
		while (c != EOF) {
			c = getc(widget->text_file);
			chars++;
			if (c == '\n' || chars >= w) {
				lines++;
				line_positions = realloc(line_positions, lines * sizeof(long));
				line_positions[lines - 1] = ftell(widget->text_file) - chars;
				if (chars > max_chars) {
					max_chars = chars;
				}
				chars = 0;
			}
		}

		int n = lines - h;
		n = n < 0 ? 0 : n;

		int left_margin = 3;
		if (max_chars < w) {
			left_margin = (w - max_chars - 0.5) / 2;
		}

		int dy = (h <= lines) ? 0 : (h - lines - 0.5) / 2;
		while (n < lines) {
			fseek(widget->text_file, line_positions[n], SEEK_SET);
			int dx = left_margin;
			int chars = 0;
			int c = 0;
			while (true) {
				c = getc(widget->text_file);
				chars++;
				if (c == '\n' || chars >= w) {
					break;
				}

				cursor_goto(x + dx, y + dy);
				putchar(c);

				dx++;
			}
			dy++;
			n++;
		}

		free(line_positions);
	} else {
		printf("no file opened\n");
	}

	widget->needs_redraw = false;
}

static void free_widget (void *_widget) {
	TextfileWidget_t *widget = _widget;
	fclose(widget->text_file);
	free(widget);
}

TextfileWidget_t *create_textfile_widget (const char *filename) {
	Widget_t *parent = create_widget(&render, &free_widget, NULL);
	parent->focusable = false;
	TextfileWidget_t *textfile_widget = realloc(parent, sizeof(TextfileWidget_t));

	textfile_widget->text_file = fopen(filename, "r");

	if (textfile_widget->text_file == NULL) {
		fprintf(stderr, "Could not open text file %s\n", filename);
		return NULL;
	}

	return textfile_widget;
}
