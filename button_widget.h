#ifndef BUTTON_WIDGET_H
#define BUTTON_WIDGET_H

#include "widget.h"

typedef struct {
	Widget_t;
	char *label;
	int label_length;
	void (*on_click)(void *);
} ButtonWidget_t;

ButtonWidget_t *create_button_widget(char *, void (*on_click)(void *));

#endif
