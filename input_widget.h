#ifndef INPUT_WIDGET_H
#define INPUT_WIDGET_H

#include <stdbool.h>
#include "widget.h"

typedef struct {
	Widget_t;
	char *buffer;
	int buffer_size;
	int buffer_length;
	int buffer_ptr;
} InputWidget_t;

InputWidget_t *create_input_widget();

#endif
