#ifndef FILL_WIDGET_H
#define FILL_WIDGET_H

#include "widget.h"

typedef struct {
	Widget_t;
	char fill_char;
} FillWidget_t;

FillWidget_t *create_fill_widget(char);

#endif
