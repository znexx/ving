#include <stdio.h>
#include <stdlib.h>
#include "term.h"

int main (void) {
	clear_screen();
	cursor_goto(0, 0);
	puts("0");
	push_origin(1, 1);
	puts("1");
	push_origin(1, 1);
	puts("2");
	push_origin(1, 1);
	puts("3");
	push_origin(1, 1);
	puts("4");
	push_origin(3, 2);
	puts("x");
	pop_origin();
	cursor_goto(2, 0);
	puts("foobar");

	putchar('\n');

	pop_origin();
	pop_origin();
	pop_origin();
	pop_origin();
	pop_origin();
	return 0;
}
