#include <stdio.h>
#include <stdlib.h>
#include "pipe_widget.h"
#include "textfile_widget.h"
#include "grid_widget.h"
#include "button_widget.h"
#include "fill_widget.h"
#include "input_widget.h"
#include "term.h"

int terminal_width = 80;
int terminal_height = 25;

InputWidget_t *input_widget2;

void button_pressed (void *_widget) {
	ButtonWidget_t *widget = _widget;
	widget = widget;
	static int n_pressed = 0;
	cursor_goto(30,30);
	printf("TJOHEJ HAHA %d %s\n", n_pressed++, input_widget2->buffer);
}

int main (void) {
	// Create a grid widget as root, with width 1 and height 3
	GridWidget_t *root_widget = create_grid_widget(1, 3);

	GridWidget_t *left_column = create_grid_widget(4, 1);
	set_grid_child(root_widget, left_column, 0, 1);

	InputWidget_t *middle = create_input_widget();
	middle->has_focus = true;
	set_grid_child(root_widget, middle, 0, 0);

	FillWidget_t *fill_widget = create_fill_widget('t');
	set_grid_child(left_column, fill_widget, 0, 0);

	InputWidget_t *input_widget1 = create_input_widget();
	set_grid_child(left_column, input_widget1, 1, 0);

	input_widget2 = create_input_widget();
	set_grid_child(left_column, input_widget2, 2, 0);

	ButtonWidget_t *button_widget = create_button_widget("Print text from field 2", &button_pressed);
	set_grid_child(left_column, button_widget, 3, 0);

	clear_screen();
	grid_focus_first(root_widget);

	root_widget->render(root_widget, 0, 0, terminal_width, terminal_height);

	root_widget->free(root_widget);

	putchar('\n');

	return 0;
}
