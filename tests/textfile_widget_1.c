#include <stdio.h>
#include <stdlib.h>
#include "textfile_widget.h"
#include "term.h"

int terminal_width = 80;
int terminal_height = 25;

int main (void) {
	TextfileWidget_t *root_widget = create_textfile_widget("tests/textfile_widget_1_input.txt");

	if (root_widget == NULL) {
		return 1;
	}

	clear_screen();

	root_widget->render(root_widget, 0, 0, terminal_width, terminal_height);

	root_widget->free(root_widget);

	putchar('\n');

	return 0;
}
