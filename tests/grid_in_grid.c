#include <stdio.h>
#include <stdlib.h>
#include "grid_widget.h"
#include "fill_widget.h"
#include "term.h"

int terminal_width = 80;
int terminal_height = 25;

int main (void) {
	GridWidget_t *root_widget = create_grid_widget(1, 3);

	GridWidget_t *left_column = create_grid_widget(3, 1);
	set_grid_child(root_widget, left_column, 0, 0);

	FillWidget_t *right_column = create_fill_widget('t');
	set_grid_child(root_widget, right_column , 0, 2);

	FillWidget_t *left_upper = create_fill_widget('x');
	set_grid_child(left_column, left_upper, 0, 0);

	clear_screen();
	grid_focus_first(root_widget);

	root_widget->render(root_widget, 0, 0, terminal_width, terminal_height);

	root_widget->free(root_widget);

	putchar('\n');

	return 0;
}
