#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "widget.h"
#include "button_widget.h"
#include "term.h"

void render (void *_widget, int x, int y, int w, int h) {
	ButtonWidget_t *widget = _widget;

	y += (h - 0.5) / 2;

	if (widget->has_focus) {
		set_negative(true);
	}

	int label_length = strlen(widget->label);
	if (label_length < w) {
		x += (w - label_length) / 2;
	}

	cursor_goto(x, y);

	for (int i = 0; i < w && widget->label[i] != '\0'; i++) {
		if (widget->label[i] != '\n') {
			putchar(widget->label[i]);
		}
	}

	if (widget->has_focus) {
		set_negative(false);
	}
}

void free_button (void *_widget) {
	ButtonWidget_t *widget = _widget;
	free(widget->label);
	free(widget);
}

void use_character (void *_widget, int character) {
	ButtonWidget_t *widget = _widget;

	if (character == '\n') {
		widget->on_click(widget);
	}

	widget = widget;
	character = character;
}

void on_click_fallback (void *_widget) {
	_widget = _widget;
}

ButtonWidget_t *create_button_widget (char *label, void (*on_click)(void *)) {
	if (on_click == NULL) {
		on_click = &on_click_fallback;
	}

	Widget_t *parent = create_widget(&render, &free_button, &use_character);
	ButtonWidget_t *widget = realloc(parent, sizeof(ButtonWidget_t));

	widget->on_click = on_click;

	// fields
	widget->label_length = strlen(label);
	widget->label = calloc(widget->label_length + 1, sizeof(char));
	strncpy(widget->label, label, widget->label_length + 1);

	return widget;
}
