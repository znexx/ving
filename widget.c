#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "widget.h"

bool step_2d(int *x, int *y, int w, int h) {
	(*x)++;
	if ((*x) >= w) {
		(*y)++;
		(*x) = 0;
		if ((*y) >= h) {
			(*y) = 0;
			return true;
		}
	}
	return false;
}

static void render_widget(void *_widget, int x, int y, int w, int h) {
	Widget_t *widget = _widget;
	widget = widget;
	x = x;
	y = y;
	w = w;
	h = h;
}

static void use_character (void *widget, int character) {
	widget = widget;
	character = character;
}

static void free_widget (void *widget) {
	free(widget);
}

Widget_t *create_widget(RenderFunction_t *provided_render, FreeFunction_t *provided_free, UseCharacterFunction_t *provided_use_character) {
	Widget_t *widget = calloc(1, sizeof(Widget_t));
	widget->type = WIDGET;
	widget->needs_redraw = true;
	widget->has_focus = false;
	widget->focusable = true;
	widget->render = provided_render ? provided_render : &render_widget;
	widget->free = provided_free ? provided_free : &free_widget;
	widget->use_character = provided_use_character ? provided_use_character : &use_character;
	return widget;
}
