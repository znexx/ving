#ifndef WIDGET_H
#define WIDGET_H

#include <stdbool.h>

typedef void callback_function(void *, void *);

typedef void RenderFunction_t(void *, int, int, int, int);
typedef void FreeFunction_t(void *);
typedef void UseCharacterFunction_t(void *, int);

typedef enum {
	WIDGET,
	GRID_WIDGET,
	INPUT_WIDGET,
	BUTTON_WIDGET,
	FILL_WIDGET
} WidgetType_t;

typedef struct {
	WidgetType_t type;
	bool needs_redraw;
	bool has_focus;
	bool focusable;
	RenderFunction_t *render;
	FreeFunction_t *free;
	UseCharacterFunction_t *use_character;
} Widget_t;

bool step_2d(int *x, int *y, int w, int h);

Widget_t *create_widget(RenderFunction_t *, FreeFunction_t *, UseCharacterFunction_t *);

#endif
