#ifndef GRID_WIDGET_H
#define GRID_WIDGET_H

#include <stdbool.h>
#include "widget.h"

typedef struct {
	Widget_t;
	int n_rows;
	int n_columns;
	Widget_t ***children;
	bool grid_drawn;
} GridWidget_t;

GridWidget_t *create_grid_widget(int, int);
void set_grid_child(GridWidget_t *, void *, int, int);
void grid_focus_first(GridWidget_t *);

#endif
